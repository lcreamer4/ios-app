exports.data = [{
	title : "Alice",
	movie : "Movie: Alice in Wonderland", 
	date: "Date Released: July 26, 1951",
	leftImage : 'DPics/Alice.jpg',
	image : 'DPics/Alice.jpg'
}, {
	title : "Ariel",
	movie: "Movie: The Little Mermaid",
	date: "Date Released: November 17, 1989",
	leftImage : 'DPics/Ariel.jpg',
	image : 'DPics/Ariel.jpg'
}, {
	title : "Aurora",
	movie: "Movie: Sleeping Beauty",
	date: "Date Released: January 29, 1959 ",
	leftImage : 'DPics/Aurora.jpg',
	image: 'DPics/Aurora.jpg'
}, {
	title : "Belle",
	movie : "Movie :Beauty and the Beast",
	date: "Date Released: November 22, 1991  ",
	leftImage : 'DPics/Belle_2.jpg',
	image: 'DPics/Belle_2.jpg'
}, {
	title : "Cinderella",
	movie : "Movie: Cinderella",
	date: "Date Released: March 4, 1950",
	leftImage : 'DPics/Cinderella.jpg',
	image: 'DPics/Cinderella.jpg'
}, {
	title : "Elsa",
	movie: "Movie: Frozen",
	date: "Date Released: November 27, 2013 ",
	leftImage : 'DPics/Elsa.jpg',
	image:'DPics/Elsa.jpg'
}, {
	title : "Esmeralda",
	movie : "Movie: The Hunchback of Notre Dame",
	date: "Date Released: December 29, 1939",
	leftImage : 'DPics/Esmeralda.jpg',
	image: 'DPics/Esmeralda.jpg'
}, {
	title : "Jane",
	movie : "Movie: Tarzan",
	date: "Date Released: June 18, 1999 ",
	leftImage : 'DPics/Jane.jpg',
	image: 'DPics/Jane.jpg'
}, {
	title : "Jasmine",
	movie : "Movie: Aladdin",
	date: "Date Released: November 25, 1992 ",
	leftImage : 'DPics/Jasmine.jpg',
	image: 'DPics/Jasmine.jpg'
}, {
	title : "Joy",
	movie : "Movie: Inside Out",
	date: "Date Released: June 19 2015 ",
	leftImage : 'DPics/Joy.jpg',
	image: 'DPics/Joy.jpg'
}, {
	title : "Kida",
	movie : "Movie: Atlantis The Lost Empire",
	date: "Date Released: June 15, 2001 ",
	leftImage : 'DPics/Kida.jpg',
	image: 'DPics/Kida.jpg'
}, {
	title : "Lilo",
	movie : "Movie: Lilo and Stitch",
	date: "Date Released: June 21, 2002 ",
	leftImage : 'DPics/Lilo.jpg',
	image: 'DPics/Lilo.jp'
}, {
	title : " Meg",
	movie :" Movie: Hercules",
	date: "Date Released: June 27, 1997 ",
	leftImage : 'DPics/Meg.jpg',
	image: 'DPics/Meg.jpg'
}, {
	title : "Merida",
	movie : "Movie: Brave",
	date: "Date Released: June 22, 2012 ",
	leftImage : 'DPics/Merida.jpg',
	image: 'DPics/Merida.jpg'
}, {
	title : "Mulan",
	movie : "Movie: Mulan",
	date: "Date Released: June 19 1998 ",
	leftImage : 'DPics/Mulan.jpg',
	image: 'DPics/Mulan.jpg'
}, {
	title : "Pocahontas",
	movie : "Movie: Pocahontas",
	date: "Date Released: June 23, 1995 ",
	leftImage : 'DPics/Pocahontas.jpg',
	image: 'DPics/Pocahontas.jpg'
}, {
	title : "Rapunzel",
	movie : "Movie: Tangled",
	date: "Date Released: November 24, 2010 ",
	leftImage : 'DPics/Rapunzel.jpg',
	image: 'DPics/Rapunzel.jpg'
}, {
	title : "Snow White",
	movie : "Movie: Snow White and the Seven Dwarves",
	date: "Date Released: December 21, 1937",
	leftImage : 'DPics/SnowWhite.jpg',
	image: 'DPics/SnowWhite.jpg'
}, {
	title : "Tiana",
	movie : "Movie: The Princess and the Frog",
	date: "Date Released: December 11, 2009 ",
	leftImage : 'DPics/Tiana.jpg',
	image: 'DPics/Tiana.jpg'
}, {
	title : "Wendy",
	movie : "Movie: Peter Pan",
	date: "Date Released: February 5, 1953 ",
	leftImage : 'DPics/Wendy.jpg',
	image: 'DPics/Wendy.jpg'
}];
