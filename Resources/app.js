// Import data
var characterData = require("character_data").data;
var detail = require("detail").getDetail;
var search = require("search").search;
// Create Window

var tableWin = Ti.UI.createWindow({
	backgroundColor : "white",
	title : "Female Disney Characters",
});

// Create Table Data Empty Array
var tableData = [];

var table = Ti.UI.createTableView({
	data : tableData,
	top : 30,
	search: search
});

// Loop through data
for (var i = 0, j = characterData.length; i < j; i++) {
	var character = characterData[i];

	// For EACH character create a row
	var row = Ti.UI.createTableViewRow({
		title : character.title,
		leftImage : character.leftImage,
		data : character
	});

	// Push the row to the table data
	tableData.push(character);
	// Set table data
	table.setData(tableData);
};

// Click Event ON the table
table.addEventListener("click", function(e) {
	var charData = e.source;
	
	var detailsWindow = detail(charData);
	
	nav.openWindow(detailsWindow);
});

// Add table to the window
tableWin.add(table);
tableWin.add(search);

var nav = Ti.UI.iOS.createNavigationWindow({
	window : tableWin
});

nav.open();
