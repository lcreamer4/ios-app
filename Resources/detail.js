var getDetail = function(charData){
	var detailWin = Ti.UI.createWindow({
		backgroundColor: "white",
		title: "Details"
	});
	
	var detailText = Ti.UI.createLabel({
		text: charData.title,
		top: 60,
	});
	
	var detailMovie = Ti.UI.createLabel({
		text: charData.movie,
		bottom: 100,
	});
	
	var detailDate = Ti.UI.createLabel({
		text:charData.date,
		bottom: 80
	});
	
	var detailPhoto = Ti.UI.createImageView({
		image: charData.image,
	});
	
	var view = Ti.UI.createView({
		backgroundColor: "white"
	});
	detailWin.add(detailText, detailMovie, detailDate, detailPhoto);

	return detailWin;
};

exports.getDetail = getDetail;